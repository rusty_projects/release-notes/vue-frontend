module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: ['plugin:vue/essential', 'eslint:recommended'],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'import/no-unresolved': 0,
        'import/no-unassigned-import': 0,
        semi: ['error', 'never'],
        'vue/no-parsing-error': [
            2,
            {
                'x-invalid-end-tag': false,
            },
        ],
        'vue/html-self-closing': 'off',
    },
    parserOptions: {
        parser: 'babel-eslint',
    },
};
