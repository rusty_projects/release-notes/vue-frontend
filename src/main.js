import Vue from 'vue'
import VueRx from 'vue-rx'
import App from './App.vue'
import router from './router'
import Config from '@/config'
import AsyncComputed from 'vue-async-computed'
import iView from 'view-design'
import locale from 'view-design/dist/locale/en-US'
import 'view-design/dist/styles/iview.css'

Vue.config.productionTip = false

Vue.prototype.$baseApiUrl = Config.API_BASE_URL

Vue.use(iView, {
  locale: locale,
})
Vue.use(VueRx)
Vue.use(AsyncComputed)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
