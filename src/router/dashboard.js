const Dashboard = () => import( /* webpackChunkName: "dashboard" */ '@/views/Dashboard')
const ReleaseNotes = () => import( /* webpackChunkName: "releasenotes" */ '@/views/ReleaseNotes')
const Version = () => import( /* webpackChunkName: "version" */ '@/views/Version')
const Search = () => import( /* webpackChunkName: "search" */ '@/views/Search')
const Settings = () => import( /* webpackChunkName: "settings" */ '@/views/Settings')
const CreateUser = () => import( /* webpackChunkName: "createuser" */ '@/views/CreateUser')
const EditUser = () => import( /* webpackChunkName: "edituser" */ '@/views/EditUser')
const CreateGroup = () => import( /* webpackChunkName: "creategroup" */ '@/views/CreateGroup')
const EditGroup = () => import( /* webpackChunkName: "editgroup" */ '@/views/EditGroup')
const ChangePassword = () => import( /* webpackChunkName: "changepassword" */ '@/views/ChangePassword')
const EditProject = () => import( /* webpackChunkName: "editproject" */ '@/views/EditProject')
const Impressum = () => import( /* webpackChunkName: "impressum" */ '@/views/Impressum')

export default [{
  path: '/',
  meta: {
    requiresAuth: true,
  },
  name: 'dashboard',
  component: Dashboard,
  children: [{
      path: 'releases',
      name: 'releases',
      meta: {
        requiresAuth: true,
      },
      component: ReleaseNotes,
      children: [{
        path: 'key/:key/version/:version',
        name: 'version',
        meta: {
          requiresAuth: true,
        },
        component: Version,
      }],
    },
    {
      path: 'search',
      name: 'search',
      meta: {
        requiresAuth: true,
      },
      component: Search,
    },
    {
      path: 'settings',
      meta: {
        requiresAuth: true,
        requiresAdmin: false
      },
      name: 'settings',
      component: Settings,
      children: [{
          path: 'createuser',
          name: 'createuser',
          meta: {
            requiresAuth: true,
          },
          component: CreateUser,
        },
        {
          path: 'edituser',
          name: 'edituser',
          meta: {
            requiresAuth: true,
          },
          component: EditUser,
        },
        {
          path: 'creategroup',
          name: 'creategroup',
          meta: {
            requiresAuth: true,
          },
          component: CreateGroup,
        },
        {
          path: 'editgroup',
          name: 'editgroup',
          meta: {
            requiresAuth: true,
          },
          component: EditGroup,
        },
        {
          path: 'editproject',
          name: 'editproject',
          meta: {
            requiresAuth: true,
          },
          component: EditProject,
        },
        {
          path: 'changepassword',
          name: 'changepassword',
          meta: {
            requiresAuth: true,
          },
          component: ChangePassword,
        },
      ]
    }, {
      path: '/impressum',
      name: 'impressum',
      component: Impressum
    }
  ],
}]