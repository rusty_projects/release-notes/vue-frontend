const Login = () => import( /* webpackChunkName: "login" */ '@/views/Login')
const Logout = () => import( /* webpackChunkName: "logout" */ '@/views/Logout')
const Datenschutz = () => import( /* webpackChunkName: "datenschutz" */ '@/views/Datenschutz')

export default [{
    path: '/login',
    name: 'login',
    component: Login
}, {
    path: '/logout',
    name: 'logout',
    component: Logout
}, {
    path: '/datenschutz',
    name: 'datenschutz',
    component: Datenschutz
}]