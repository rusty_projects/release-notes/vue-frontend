import Vue from 'vue'
import Router from 'vue-router'
import Public from '@/router/public'
import VueRouter from 'vue-router'
import Guard from '@/router/routerGuard'
import Dashboard from '@/router/dashboard'

Vue.use(Router)

const routes = [...Dashboard, ...Public]

let router = new VueRouter({
  mode: 'history',
  base: '/',
  routes,
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    Guard.checkLogin().then(result => {
      if (!result) {
        next({
          path: '/login',
          query: {
            redirect: to.fullPath,
          },
        })
      } else {
        next()
      }
    })
  } else {
    next()
  }
})

export default router
