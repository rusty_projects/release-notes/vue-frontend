import Config from '@/config'
import axios from 'axios'

async function checkLogin() {
  try {
    await axios.get(Config.API_BASE_URL + '/auth')
    return true
  } catch (error) {
    console.error(error)
    return false
  }
}

async function checkAdmin() {
  try {
    const response = await axios.get(Config.API_BASE_URL + '/auth')
    return response.data.admin
  } catch (error) {
    console.error(error)
    return false
  }
}

export default {
  checkLogin,
  checkAdmin,
}
